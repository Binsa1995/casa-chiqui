const fileinclude = require('gulp-file-include');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const gulp = require('gulp');

gulp.task('sass', () => {
    return gulp.src([
        'node_modules/bootstrap/scss/bootstrap.scss',
        'node_modules/slick-carousel/slick/slick.scss',
        'node_modules/aos/src/sass/aos.scss',
        'node_modules/hover.css/scss/*.scss',
        'src/scss/**/*.scss'
        //'src/scss/nodes/*.scss'
    ])
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());
});

gulp.task('js', () => {
    return gulp.src([
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/jquery-ui/ui/jquery-1-7.js',
        'node_modules/aos/dist/aos.js',
        'node_modules/popper.js/dist/umd/popper.min.js',
        'node_modules/slick-carousel/slick/slick.min.js',
        'src/js/*.js'
    ])
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.stream());
});

gulp.task('copy-media', () => {
    return gulp.src([
        'src/media/img/*',
        'src/media/*'
    ])
        .pipe(gulp.dest('./dist/media/'))
        .pipe(browserSync.stream());
});

gulp.task('fuentes', () => {
    return gulp.src('src/fonts/*')
        .pipe(gulp.dest('dist/media'));
});

gulp.task('fileinclude', () => {
    return gulp.src(['src/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.reload({ stream: true }))
});

gulp.task('serve', ['fileinclude', 'js', 'sass'], () => {
    browserSync.init({
        server: './dist'
    });
    gulp.watch([
        'node_modules/bootstrap/scss/bootstrap.min.scss',
        //'src/scss/nodes/*.scss',
        'src/scss/**/*.scss',
        'src/*.html',
        'src/nodes/*.html',
        'src/js/*.js'
    ], ['fileinclude', 'js', 'sass']);

    gulp.watch('./dist/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['js', 'copy-media', 'fuentes', 'fileinclude', 'serve'])