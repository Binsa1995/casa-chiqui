$(document).ready(function () {
    makeBg();
    toggleTarget();
    gotoBtn();
    //hamburger();
    //headerDrops();
    search();
    slidersAbout();
    slidersdetail();
    setupHeader();
    toggleMenu();
    filtros()
    faq()
    Accdetalle()
    //bannerHome();
    AOS.init({ duration: 400, once: true, disable: 'mobile' });

    $('.inputNumber-plus').click(inpNum.countUp);
    $('.inputNumber-minus').click(inpNum.countDown);
});

/*Canbtidad*/

var inpNum = {
    countUp: function (e) {
        e.preventDefault();
        var $input = $(this).closest('.inputNumber').find('input');
        $input.val(parseInt($input.val()) + 1);
    },
    countDown: function (e) {
        e.preventDefault();
        var $input = $(this).closest('.inputNumber').find('input');
        if (parseInt($input.val()) > 1) {
            $input.val(parseInt($input.val()) - 1);
        }
    }
};

$(document).on('scroll', function () {
    gtobtnscroll();
});

function makeBg() {
    $('.makeBg').each(function () {
        if ($(this).find('img').length) {
            var theBgPic = $(this).children('img').first();
            theBgPic.hide();
            var bgSrc = "url('" + theBgPic.attr('src') + "')";
            $(this).css('background-image', bgSrc);
        }
    });
}

function toggleTarget() {
    $('.toggleAnchor').click(function (e) {
        e.preventDefault();
        var disBtn = $(this);
        var targett = disBtn.attr('href');
        $(targett).slideToggle('fast', function () {
            if ($(targett).is(':visible')) {
                disBtn.addClass('active');
            } else {
                disBtn.removeClass('active');
            }
        });
    })
}

/*otro gotop*/
function gotoBtn() {
    $('.goTo').click(function () {
        var targ = $(this).attr('href');
        var pos = $(targ).offset().top - 40;
        $("html, body").animate({ scrollTop: pos }, 700);
    });
}

/*preloader*/
function Preloader(msj) {
    $('#preloader .preloader-text').text('');
    if (msj != '') {
        $('#preloader .preloader-text').text(msj)
    }
    if ($('#preloader').is(':visible')) {
        $('#preloader').fadeOut('fast')
    } else {
        $('#preloader').fadeIn(400)
    }
}

function hideDrops(btn, drop) {
    $('.mainmenu-link, .opts-link').not(btn).removeClass('active');
    $('.header_drop').not(drop).hide();
}

function alerta(msj) {
    $('#alertModal').modal('toggle');
    if (msj != '') {
        $('#alert-msj').text(msj);
    } else {
        $('#alert-msj').text('¿Estás seguro que deseas continuar?');
    }
}

function hamburger() {
    $('.hamburger').on('click', function (e) {
        e.preventDefault();
        var dis = $(this);
        var mainmenu = $('.mainNav');
        if (mainmenu.is(':visible')) {
            dis.removeClass('active');
        } else {
            dis.addClass('active');
        }
        mainmenu.slideToggle(500);
    });
}

function headerDrops() {
    $('.no-touchevents .mainmenu_link').on('mouseenter', function (e) {
        var disBtn = $(this);
        var disDad = disBtn.closest('.mainmenu_item');
        var disDrop = disDad.find('.mainmenu_drop');
        $('.mainmenu_drop').not(disDrop).slideUp();
        $('.mainmenu_item').not(disBtn).removeClass('active');
        if (disDrop.length > 0) {
            e.preventDefault();
            disBtn.addClass('active');
            disDrop.slideDown('fast');
        }
        hideDrops(disBtn, disDrop);
    }).mouseleave(function (e) {
        var disBtn = $(this);
        var disDad = disBtn.closest('.mainmenu_item');
        var disDrop = disDad.find('.mainmenu_drop');
        var disDropOver = disDad.find('.head_prod__menu');
        setTimeout(function () {
            if ($('.mainmenu_drop:hover').length) { }
            else if (disBtn.is(':hover')) { }
            else {
                disDrop.slideUp();
                disBtn.removeClass('active');
            }
        }, 200);

    });

    $('.no-touchevents .head_prod__menu').on('mouseleave', function (e) {
        var disDropOver = $(this);
        var disDad = disDropOver.closest('.mainmenu_item');
        var disBtn = disDad.find('.mainmenu_link');
        var disDrop = disDropOver.closest('.mainmenu_drop');
        setTimeout(function () {
            if ($('.head_prod__menu:hover').length) {
            } else if ($('.mainmenu_link:hover').length) {
            } else {
                disDrop.slideUp();
                disBtn.removeClass('active');
            }
        }, 200);

    });

    $('.touchevents .mainmenu_link').on('click', function (e) {
        var disBtn = $(this);
        var disDad = disBtn.closest('.mainmenu_item');
        var disDrop = disDad.find('.mainmenu_drop');
        $('.mainmenu_drop').not(disDrop).slideUp();
        $('.mainmenu_item').not(disBtn).removeClass('active');
        if (disDrop.length > 0 && disBtn.hasClass('active')) {
            e.preventDefault();
            disBtn.removeClass('active');
            disDrop.slideUp('fast');
        } else if (disDrop.length > 0) {
            e.preventDefault();
            disBtn.addClass('active');
            disDrop.slideDown('fast');
        }
        hideDrops(disBtn, disDrop);
    });
}

function search() {
    $('.search, .searchBox-close, .header__action-group .fa-search').on('click', function (e) {
        e.preventDefault();
        var dis = $(this);
        var mainmenu = $('.searchBox');
        if (mainmenu.is(':visible')) {
            dis.removeClass('active');
            $('.oficial').css('display', 'block');
        } else {
            dis.addClass('active');
            $('.oficial').css('display', 'none');
        }
        mainmenu.toggle("slide", 20);
    });
}

/*slick */
function slidersAbout() {
    $('.slickCategorias').slick({
        dots: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,

        prevArrow: '<button type="button" class="slick-prev"><i class="icon-arrow-right"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="icon-arrow-left"></i></button>',
        responsive: [
            {
                breakpoint: 790,
                settings: {
                    autoplay: true,
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 575,
                settings: {
                    autoplay: true,
                    slidesToShow: 1,
                }

            }]
    });
}

/*slick detail*/
function slidersdetail() {
    $('.slickdetail').slick({
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,

        prevArrow: '<button type="button" class="slick-prev"><i class="icon-arrow-right"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="icon-arrow-left"></i></button>',
        responsive: [
            {
                breakpoint: 575,
                settings: {
                    autoplay: true,
                    dots: true,
                }
            }]
    });
}

/*menu de navegavion */
function setupHeader() {
    if (window.location.pathname !== '/') {
        $('.header').addClass('header--no-index');
    }

    $('.nav-tabs > li > a').mouseenter(function () {
        $(this).tab('show');
    });
    $('.header__main-nav').mouseleave(function () {
        // $('.tab-pane.active.show').removeClass('active').removeClass('show');
        // $('.header__nav-item.active').removeClass('active');
    });

    $('.header__nav-bar-item a').mouseenter(function () {
        const target = $(this).data('collapse');
        $(`#${target}`).collapse('show');
    });

    $('.header__nav-bar-item').mouseleave(function () {
        const target = $(this).find('a').data('collapse');
        $(`#${target}`).collapse('hide');
    });
}

function bannerHome() {
    $('.mainBanner_slick').slick({
        dots: false,
        slidesToShow: 1,
        fade: true,
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="slick-prev"><i class="icon-arrow-right"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="icon-arrow-left"></i></button>'
    });

    $('.lista_mainSlider__rare').slick({
        adaptiveHeight: true,
        arrows: false,
        dots: false,
        vertical: true,
        slidesToShow: 3,
        asNavFor: '.mainSlider_rare',
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    vertical: true,
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.mainSlider_rare').slick({
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        asNavFor: '.lista_mainSlider__rare',
        focusOnSelect: true,
        arrows: false,
        prevArrow: '<button type="button" class="slick-prev"><i class="icon-arrow-right"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="icon-arrow-left"></i></button>'
    });
}

/*cambia de menu*/
function toggleMenu() {
    $('.header__icon-menu').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('fa-bars').toggleClass('fa-times');
        $('.header__menu').toggleClass('transparent').toggleClass('opaque');
        $('.header__icon').toggleClass('icon--white').toggleClass('icon--black');
        $('.header__language').toggleClass('icon--white').toggleClass('icon--black');
    });
}


/*accordeones*/

function faq() {

    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var respuesta = this.nextElementSibling;
            if (respuesta.style.maxHeight) {
                respuesta.style.maxHeight = null;
            } else {
                respuesta.style.maxHeight = respuesta.scrollHeight + "px";
            }
        });
    }
}

/*accordeon de detalle*/

function Accdetalle() {

    var acc = document.getElementsByClassName("accordionDetalle");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
}

/*Accordeon de filtros*/
function filtros() {

    var acc = document.getElementsByClassName("despliega");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var bloque = this.nextElementSibling;
            if (bloque.style.display === "block") {
                bloque.style.display = "none";
            } else {
                bloque.style.display = "block";
            }
        });
    }

    document.getElementById('btn-filtro').addEventListener('click', () => {
        document.querySelector('.subcategorias__contenedor-filtro').classList.toggle('expanded');
    });

    document.getElementById('cerrar_filtro').addEventListener('click', () => {
        document.querySelector('.subcategorias__contenedor-filtro').classList.toggle('expanded');
    });
}


/*intagram*/

function instagramPlugin() {
    // if ($('#instafeed').length) {
    var feed = new Instafeed({
        get: 'user',
        userId: '4220934672',
        accessToken: '4220934672.1677ed0.b4b37ca5bfbc490496c0843eeafc6b22',
        clientId: '54c592f3e71642ec94ac5428df2c4988',
        template: '<a class="instafeed-item" href="{{link}}" target="_blank"><img src="{{image}}" alt="Instagram"></a>',
        limit: 4,
        resolution: 'low_resolution'
    });
    feed.run();
    console.log('instafeed');
    // }
}

/*boton Up*/

function gtobtnscroll() {
    var posi = $(window).scrollTop();
    if (posi > 500) {
        $('.bottoTop').stop().fadeIn(500);
    }
    else {
        $('.bottoTop').stop().fadeOut(500);
    }
}

